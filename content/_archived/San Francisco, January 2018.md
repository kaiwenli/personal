+++
title = "San Francisco, January 2018"
date = 2018-01-05T01:10:48-08:00
draft = false
series = "blog"
+++

The day after New Year's I went to SF with my family. Took BART to Powell St. and then made our way to Barney's, a boutique clothing store. I liked looking at rich teenagers trying out Bape and having their moms check out for them; interesting juxtaposition. I tried on the coveted geobaskets by Rick Owens and some gaudy dog t-shirt that was Givenchy-rottweiler like. It was fun.

{{< imgy "powell" "/images/blog/San Francisco, January 2018/full/DSCF4108.JPG" "/images/blog/San Francisco, January 2018/thumb/DSCF4108.JPG" >}}

We picked up some burgers shortly afterwards. After that, we decided to walk to Painted Ladies, passing by some unique buildings on the way there.

{{< imgy "house" "/images/blog/San Francisco, January 2018/full/DSCF4111.JPG" "/images/blog/San Francisco, January 2018/thumb/DSCF4111.JPG" >}}

I really liked the pastel colors of Painted Ladies. The washed out colors complement SF's cloudy skyline well.

{{< imgy "house" "/images/blog/San Francisco, January 2018/full/DSCF4115.JPG" "/images/blog/San Francisco, January 2018/thumb/DSCF4115.JPG" >}}

{{< imgy "house" "/images/blog/San Francisco, January 2018/full/DSCF4123.JPG" "/images/blog/San Francisco, January 2018/thumb/DSCF4123.JPG" >}}

We went to Twin Peaks afterwards. Looking at the city from atop was neat. I took pics there but I don't really like how they turned out.

Past that, we sat around at a coffee shop for a bit and then some dumpling shop. The dumpling I ordered was more of a novelty menu item. It was a huge dumpling and it came with a straw to suck out the soup but the flavor in itself wasn't anything too special.

┐('～`)┌

Overall it was neat little trip though. Nice to get out of the house with family.
