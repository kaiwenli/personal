+++
title = "Ghost Graffiti"
date = 2019-01-04T16:02:31-08:00
draft = false
series = "blog"
+++

Around Harvard's campus exists a ghost-like fella, painted on various street items like trash cans. After noticing I bump into this guy pretty frequently, I came to realize it's not painted only in one place. I then started taking pictures of the different places where Ghostyboi could be found.

One of the two below is taken on the intersection between Holyoke St and Mt Auburn St. Not sure where the other one was taken other than the fact it's somewhere "on campus".

{{< imgy "recycling" "/images//blog/Ghost Graffiti/full/recycling.jpg" "/images//blog/Ghost Graffiti/thumb/recycling.jpg" >}}

{{< imgy "trash" "/images/blog/Ghost Graffiti/full/trash.jpg" "/images/blog/Ghost Graffiti/thumb/trash.jpg" >}}

To my surprise I found that Ghostyboi can be found outside of Cambridge! After walking back from some shawarma place, I noticed it painted on a wall in Allston. I took this picture below on Cambridge St where it goes over the Mass Turnpike. Another difference with this representation is that Ghostyboi's body is filled in and it looks like it might be sweating. Guess it's on the run.

{{< imgy "allston" "/images/blog/Ghost Graffiti/full/allston.jpg" "/images/blog/Ghost Graffiti/thumb/allston.jpg" >}}

Ghostyboi isn't always well received though. I intended on taking another picture to add to my collection since I saw that it was painted on a structure on Linden St but found that it was painted over. Though if looked at closely, it can still be faintly seen. I'll update with a picture when I get back on campus.

A bit after originally writing this post, I stumbled upon a {{< linky "https://www.reddit.com/r/boston/comments/8s0fre/whats_the_deal_with_this_ghost_tag_i_keep_seeing/" "post on /r/boston" >}} that asked about "this ghost tag". According to the comments on that post, Ghostyboi can be found along "Fitchburg commuter rail line" and even as north as Concord, NH (about 70 miles away from Cambridge).

Another comment in the same post claimed to know what the ghost is: "It's Basquiat's ghost. The jaggedy part is adapted from his crown tag." Though there are some similarities between Basquiat's crown and Ghostyboi's shape, I'm slightly skeptical if that is the true origin.

Here are some more links with information about this ghost:

<ul class="list">
<li>A {{< linky "https://www.flickr.com/photos/131512700@N02/sets/72157675499060911" "Flickr" >}} with pictures of variations of Ghostyboi. Main difference between some of the pictures here and mine is that most of these ghosts only have one eye whereas mine have two slanted ones.</li>
<li>{{< linky "https://www.flickr.com/photos/131512700@N02/sets/72157675499060911" "Another Flickr" >}} which refers to the one-eyed ghost as "One Eyed Space Alien".</li>
<li>One {{< linky "https://www.reddit.com/r/boston/comments/4vid6e/what_is_this_alien_spray_paint_tag/d5yqm4h/" "user on /r/boston" >}} thinks the tag comes from a hip hop group called Dilated Peoples. I think this one is closer than the theory regarding Basquiat.</li>
<li>A {{< linky "https://twitter.com/dsparks/status/803712648848306176" "Twitter thread" >}} with lots of pictures of the "ghost alien". Cities/neighborhoods where it can be found include Quincy, Somerville, Waltham, and Watertown.</li>
</ul>

Hopefully I stumble upon more information about Ghostyboi and its relatives!

Update: 10.21.2018, posting some pics from the previous month. The former pic is on Mass Ave near Central Square, the latter pic is near Johnston Gate.

{{< imgy "exit" "/images/blog/Ghost Graffiti/full/exit.jpg" "/images/blog/Ghost Graffiti/thumb/exit.jpg" >}}

{{< imgy "postage" "/images/blog/Ghost Graffiti/full/postage.jpg" "/images/blog/Ghost Graffiti/thumb/postage.jpg" >}}
