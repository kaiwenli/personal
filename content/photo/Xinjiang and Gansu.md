+++
title = "Xinjiang and Gansu"
date = 2019-01-05T01:18:08-08:00
draft = false
series = "photo"
+++

{{< tiny >}} There was a lot to see in Xinjiang and Gansu. This pertained mostly to the outdoors, where the scenery and sights were very sublime. {{< /tiny >}}

{{< imgy "boat" "/images/photo/Xinjiang and Gansu/full/DSCF3638.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3638.JPG" >}}
{{< imgy "water" "/images/photo/Xinjiang and Gansu/full/DSCF3666.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3666.JPG" >}}
{{< imgy "temple" "/images/photo/Xinjiang and Gansu/full/DSCF3671.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3671.JPG" >}}
{{< imgy "cow bone" "/images/photo/Xinjiang and Gansu/full/DSCF3675.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3675.JPG" >}}
{{< imgy "totem thingy" "/images/photo/Xinjiang and Gansu/full/DSCF3694.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3694.JPG" >}}
{{< imgy "neat pattern" "/images/photo/Xinjiang and Gansu/full/DSCF3704.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3704.JPG" >}}
{{< imgy "yurt" "/images/photo/Xinjiang and Gansu/full/DSCF3712.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3712.JPG" >}}
{{< imgy "yurt artsy" "/images/photo/Xinjiang and Gansu/full/DSCF3714.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3714.JPG" >}}
{{< imgy "goats" "/images/photo/Xinjiang and Gansu/full/DSCF3730.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3730.JPG" >}}
{{< imgy "hillside" "/images/photo/Xinjiang and Gansu/full/DSCF3752.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3752.JPG" >}}
{{< imgy "desert" "/images/photo/Xinjiang and Gansu/full/DSCF3760.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3760.JPG" >}}
{{< imgy "camels" "/images/photo/Xinjiang and Gansu/full/DSCF3773.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3773.JPG" >}}
{{< imgy "people in desert" "/images/photo/Xinjiang and Gansu/full/DSCF3779.JPG" "/images/photo/Xinjiang and Gansu/thumb/DSCF3779.JPG" >}}
