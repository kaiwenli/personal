+++
title = "Xi'an"
date = 2019-01-05T01:18:01-08:00
draft = false
series = "photo"
+++

{{< tiny >}} I travelled to Xi'an in the summer of 2017. It was my first time there. We went to some historic sites, climbed a mountain, and tried regional cuisines. {{< /tiny >}}

{{< imgy "night market" "/images/photo/Xi'an/full/20170602_223512.jpg" "/images/photo/Xi'an/thumb/20170602_223512.jpg" >}}
{{< imgy "rouburger" "/images/photo/Xi'an/full/20170603_191930.jpg" "/images/photo/Xi'an/thumb/20170603_191930.jpg" >}}
{{< imgy "plant" "/images/photo/Xi'an/full/DSCF3548.JPG" "/images/photo/Xi'an/thumb/DSCF3548.JPG" >}}
{{< imgy "traffic" "/images/photo/Xi'an/full/DSCF3551.JPG" "/images/photo/Xi'an/thumb/DSCF3551.JPG" >}}
{{< imgy "temple" "/images/photo/Xi'an/full/DSCF3564.JPG" "/images/photo/Xi'an/thumb/DSCF3564.JPG" >}}
{{< imgy "pagoda" "/images/photo/Xi'an/full/DSCF3575.JPG" "/images/photo/Xi'an/thumb/DSCF3575.JPG" >}}
{{< imgy "mountains" "/images/photo/Xi'an/full/DSCF3577.JPG" "/images/photo/Xi'an/thumb/DSCF3577.JPG" >}}
{{< imgy "ribbons" "/images/photo/Xi'an/full/DSCF3592.JPG" "/images/photo/Xi'an/thumb/DSCF3592.JPG" >}}
{{< imgy "gate" "/images/photo/Xi'an/full/DSCF3609.JPG" "/images/photo/Xi'an/thumb/DSCF3609.JPG" >}}
