+++
title = "Road Trip to Southern California"
date = 2019-01-05T01:19:09-08:00
draft = false
series = "photo"
+++

{{< tiny >}} Our trip started off pretty foggy. We first made our way around the 17-Mile Drive in Pebble Beach. As we continued down south along Highway 1, the skies became clearer. {{< /tiny >}}

{{< imgy "DSCF4450" "/images/photo/Road Trip to Southern California/full/DSCF4450.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4450.JPG" >}}
{{< imgy "DSCF4476" "/images/photo/Road Trip to Southern California/full/DSCF4476.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4476.JPG" >}}

{{< tiny >}} The ocean was very pretty. {{< /tiny >}}

{{< imgy "DSCF4529" "/images/photo/Road Trip to Southern California/full/DSCF4529.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4529.JPG" >}}
{{< imgy "DSCF4548" "/images/photo/Road Trip to Southern California/full/DSCF4548.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4548.JPG" >}}
{{< imgy "DSCF4661" "/images/photo/Road Trip to Southern California/full/DSCF4661.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4661.JPG" >}}

{{< tiny >}} On the second day we stopped by Santa Barbara and saw the Spanish mission there. {{< /tiny >}}

{{< imgy "DSCF4677" "/images/photo/Road Trip to Southern California/full/DSCF4677.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4677.JPG" >}}

{{< tiny >}} There was also this alleyway in downtown Santa Barbara that I thought was pretty neat. The buildings felt very Spanish and Cuban to me, mainly because of the white exteriors and ceramic tile roofs. {{< /tiny >}}

{{< imgy "DSCF4712" "/images/photo/Road Trip to Southern California/full/DSCF4712.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4712.JPG" >}}
{{< imgy "DSCF4721" "/images/photo/Road Trip to Southern California/full/DSCF4721.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4721.JPG" >}}
{{< imgy "DSCF4722" "/images/photo/Road Trip to Southern California/full/DSCF4722.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4722.JPG" >}}
{{< imgy "DSCF4724" "/images/photo/Road Trip to Southern California/full/DSCF4724.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4724.JPG" >}}
{{< imgy "DSCF4725" "/images/photo/Road Trip to Southern California/full/DSCF4725.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4725.JPG" >}}
{{< imgy "DSCF4729" "/images/photo/Road Trip to Southern California/full/DSCF4729.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4729.JPG" >}}
{{< imgy "DSCF4730" "/images/photo/Road Trip to Southern California/full/DSCF4730.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4730.JPG" >}}
{{< imgy "DSCF4731" "/images/photo/Road Trip to Southern California/full/DSCF4731.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4731.JPG" >}}

{{< tiny >}} Santa Monica was alright overall, but traffic sucked. Though I suppose that's pretty emblematic of the LA metro area. {{< /tiny >}}

{{< imgy "DSCF4761" "/images/photo/Road Trip to Southern California/full/DSCF4761.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4761.JPG" >}}

{{< tiny >}} LA Public Library's mural is really nice. {{< /tiny >}}

{{< imgy "DSCF4797" "/images/photo/Road Trip to Southern California/full/DSCF4797.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4797.JPG" >}}
{{< imgy "DSCF4798" "/images/photo/Road Trip to Southern California/full/DSCF4798.JPG" "/images/photo/Road Trip to Southern California/thumb/DSCF4798.JPG" >}}

