# personal
Personal site. Utilizes [Hugo](https://gohugo.io/) and [Tachyons](http://tachyons.io/).

## Shortcodes
  - `imgy`: description, full path, thumb path
  - `tiny`: text for photoessays
    - I can probably just patch this with better CSS
