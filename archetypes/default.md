+++
title = "{{ replace .Name "-" " " | title }}"
date = {{ .Date }}
draft = true
series = "{{ replace .Dir "/" "" }}"
+++
